#ifndef GRAVITY_TANK_H
#define GRAVITY_TANK_H

#include <iostream>

template <class T>
struct GravityTank {
private:
    T* storage;
    int capacity;
    int count;

public:
    // default constructor
    GravityTank(){
        // std::cout << "default constructor" << std::endl;
        capacity = 1;
        count = 0;
        storage = new T[capacity];
    };

    // copy constructor
    GravityTank(const GravityTank<T>& other) {
        // std::cout << "copy constructor" << std::endl;
        capacity = other.capacity;
        count = other.count;
        storage = new T[capacity];

        for (int i = 0; i < count; i++) {
            storage[i] = other.storage[i];
        }
    }

    GravityTank<T>& operator=(const GravityTank<T>& other) {
        // std::cout << "overloaded assignment operator" << std::endl;

        if (capacity == other.capacity) {
            count = other.count;

            for (int i = 0; i < count; i++) {
                storage[i] = other.storage[i];
            }
        } else {
            delete[] storage;

            capacity = other.capacity;
            count = other.count;
            storage = new T[capacity];

            for (int i = 0; i < count; i++) {
                storage[i] = other.storage[i];
            }
        }

        return *this;
    }

    // overloadind the greater than operator
    bool operator>(const GravityTank<T>& other) {
        return count > other.count;
    }

    void insert(T x) {
        storage[count] = x;
        count++;

        if (count == capacity) {
            capacity *= 2;

            T* temp = new T[capacity];
            for (int i = 0; i < count; i++) {
                temp[i] = storage[i];
            }

            T* old = storage;
            storage = temp;
            delete[] old;
        }

        // placing last element in correct position
        int curr = count - 1;
        while(curr > 0 && storage[curr - 1] > storage[curr]) {
            T temp = storage[curr];
            storage[curr] = storage[curr - 1];
            storage[curr - 1] = temp;
            curr--;
        }
    }

    int getCount() const {
        return count;
    }

    int getCapacity() const {
        return capacity;
    }

    T getItem(int index) const {
        return storage[index];
    }

    // destructor
    ~GravityTank(){
        // std::cout << "destructor" << std::endl;
        delete[] storage;
    }

    // template <class Type>
    // friend std::ostream& operator<<(std::ostream&, const GravityTank<Type>&);
};

// overloads the shift left operator
template <class T>
std::ostream& operator<<(std::ostream& os, const GravityTank<T>& tank) {
    os << "Capacity: " << tank.getCapacity() << std::endl;
    os << "Count: " << tank.getCount() << std::endl;
    os << "Tank: [";

    for (int i = 0; i < tank.getCount(); i++) {
        os << tank.getItem(i);
        if (i < tank.getCount() - 1) {
            os << ", ";
        }
    }
    os << "]";
    return os;
}

#endif